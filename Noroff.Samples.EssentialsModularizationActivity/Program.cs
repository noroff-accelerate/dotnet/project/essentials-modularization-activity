﻿namespace Noroff.Samples.EssentialsModularizationActivity
{
    // Defining the MathOperations class
    public class MathOperations
    {
        // Method overloading for the 'Add' method

        // Add method for two integers
        public static int Add(int a, int b)
        {
            return a + b;
        }

        // Add method for three doubles
        public static double Add(double a, double b, double c)
        {
            return a + b + c;
        }

        // Add method using 'params' to accept a variable number of integers
        public static int Add(params int[] numbers)
        {
            return numbers.Sum();
        }

        // Method for calculating factorial using recursion
        public static int Factorial(int n)
        {
            if (n == 0 || n == 1)
            {
                return 1;
            }
            else
            {
                return n * Factorial(n - 1);
            }
        }

        // Method for calculating factorial using a loop
        public static int FactorialLoop(int n)
        {
            int result = 1;
            for (int i = 2; i <= n; i++)
            {
                result *= i;
            }
            return result;
        }
    }

    // Example usage in a Main method
    public class Program
    {
        public static void Main(string[] args)
        {
            // Adding two integers
            int addResult1 = MathOperations.Add(5, 10);

            // Adding three doubles
            double addResult2 = MathOperations.Add(1.2, 3.4, 5.6);

            // Adding a variable number of integers
            int addResult3 = MathOperations.Add(1, 2, 3, 4, 5);

            // Calculating factorial using recursion
            int factorialResult = MathOperations.Factorial(5);

            // Calculating factorial using a loop
            int factorialLoopResult = MathOperations.FactorialLoop(5);

            // Printing results
            Console.WriteLine("Add Result 1 (Two Integers): " + addResult1);
            Console.WriteLine("Add Result 2 (Three Doubles): " + addResult2);
            Console.WriteLine("Add Result 3 (Variable Number): " + addResult3);
            Console.WriteLine("Factorial (Recursion): " + factorialResult);
            Console.WriteLine("Factorial (Loop): " + factorialLoopResult);
        }
    }

}