# Math Operations Activity

This activity demonstrates key concepts in C# programming, including method overloading, the use of the `params` keyword, and understanding the differences between recursion and loops.

## Overview

The `MathOperations` class contains several methods that illustrate these concepts:

- Overloaded `Add` methods for adding numbers.
- A `Factorial` method implemented using recursion.
- A `FactorialLoop` method implemented using a loop.

## Implementation

### Class: MathOperations

#### Methods

1. **Add (Method Overloading)**
   - Overloads for adding two integers, three doubles, and a variable number of integers using `params`.
2. **Factorial (Recursion)**
   - Calculates the factorial of a number using recursion.
3. **FactorialLoop (Loop)**
   - Calculates the factorial of a number using a loop.

### Usage

In the `Program` class's `Main` method, examples of each method's usage are demonstrated.

### Example Output

The program will output the results of the various `Add` methods, as well as the factorial calculations from both the recursive and loop-based implementations.

## Running the Program

1. Compile the C# code.
2. Run the resulting executable to see the output of the operations performed in the `Main` method.

## Learning Objectives

By completing this activity, learners will demonstrate their understanding of:

- The use of method overloading in C#.
- Simplifying methods using the `params` keyword.
- The practical differences and applications of recursion and loops in C#.

## Conclusion

This activity provides hands-on experience with important C# programming concepts, aiding in the understanding of how to write more efficient and effective code.
